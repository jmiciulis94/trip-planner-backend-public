CREATE TABLE city (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    name TEXT NOT NULL,
    sequence INTEGER NOT NULL,
    southwest_latitude NUMERIC(7, 5) NOT NULL,
    southwest_longitude NUMERIC(7, 5) NOT NULL,
    northeast_latitude NUMERIC(7, 5) NOT NULL,
    northeast_longitude NUMERIC(7, 5) NOT NULL,
    schedules_url TEXT NOT NULL,
    gps_url TEXT
);

CREATE TABLE city_transport_type (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    city_id BIGINT NOT NULL REFERENCES city (id),
    transport_type VARCHAR(20) NOT NULL CHECK (transport_type IN ('BUS', 'EXPRESS_BUS', 'NIGHT_BUS', 'TROLLEYBUS', 'MINIBUS', 'DISTRICT_BUS')),
    color VARCHAR(9) NOT NULL,
    UNIQUE (city_id, transport_type)
);

CREATE INDEX ON city_transport_type (city_id);

CREATE TABLE route (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    city_id BIGINT NOT NULL REFERENCES city (id),
    code TEXT NOT NULL UNIQUE,
    transport_type VARCHAR(20) NOT NULL CHECK (transport_type IN ('BUS', 'EXPRESS_BUS', 'NIGHT_BUS', 'TROLLEYBUS', 'MINIBUS', 'DISTRICT_BUS')),
    number VARCHAR(100) NOT NULL,
    title TEXT NOT NULL,
    valid_since DATE,
    version BIGINT NOT NULL,
    version_date DATE NOT NULL
);

CREATE INDEX ON route (city_id);

CREATE TABLE route_alias (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    code TEXT NOT NULL UNIQUE,
    title TEXT NOT NULL,
    alias TEXT NOT NULL
);

CREATE TABLE stop (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    city_id BIGINT NOT NULL REFERENCES city (id),
    code BIGINT NOT NULL,
    title TEXT NOT NULL,
    latitude NUMERIC(7, 5) NOT NULL,
    longitude NUMERIC(7, 5) NOT NULL,
    version BIGINT NOT NULL,
    UNIQUE (city_id, code)
);

CREATE INDEX ON stop (city_id);

CREATE TABLE direction (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    route_id BIGINT NOT NULL REFERENCES route (id),
    code TEXT NOT NULL UNIQUE,
    title TEXT NOT NULL,
    sequence INTEGER NOT NULL,
    number_of_trips INTEGER NOT NULL
);

CREATE INDEX ON direction (route_id);

CREATE TABLE direction_alias (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    code TEXT NOT NULL UNIQUE,
    title TEXT NOT NULL,
    alias TEXT
);

CREATE TABLE direction_stop (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    direction_id BIGINT NOT NULL REFERENCES direction (id),
    stop_id BIGINT NOT NULL REFERENCES stop (id),
    sequence INTEGER NOT NULL
);

CREATE INDEX ON direction_stop (direction_id);
CREATE INDEX ON direction_stop (stop_id);

CREATE TABLE direction_shape (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    direction_id BIGINT NOT NULL REFERENCES direction (id),
    latitude NUMERIC(8, 6) NOT NULL,
    longitude NUMERIC(8, 6) NOT NULL,
    sequence INTEGER NOT NULL
);

CREATE INDEX ON direction_shape (direction_id);

CREATE TABLE stop_time (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    direction_stop_id BIGINT NOT NULL REFERENCES direction_stop (id),
    trip_code VARCHAR(100) NOT NULL,
    monday BOOLEAN NOT NULL,
    tuesday BOOLEAN NOT NULL,
    wednesday BOOLEAN NOT NULL,
    thursday BOOLEAN NOT NULL,
    friday BOOLEAN NOT NULL,
    saturday BOOLEAN NOT NULL,
    sunday BOOLEAN NOT NULL,
    time TIME NOT NULL
);

CREATE INDEX ON stop_time (direction_stop_id);

CREATE TABLE terms_and_conditions (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    version INTEGER NOT NULL,
    header_en TEXT NOT NULL,
    header_lt TEXT NOT NULL,
    footer_en TEXT NOT NULL,
    footer_lt TEXT NOT NULL
);

CREATE TABLE terms_and_conditions_chapter (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    terms_and_conditions_id BIGINT NOT NULL REFERENCES terms_and_conditions (id),
    title_en TEXT NOT NULL,
    title_lt TEXT NOT NULL,
    sequence INTEGER NOT NULL
);

CREATE INDEX ON terms_and_conditions_chapter (terms_and_conditions_id);

CREATE TABLE terms_and_conditions_paragraph (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    terms_and_conditions_chapter_id BIGINT NOT NULL REFERENCES terms_and_conditions_chapter (id),
    text_en TEXT NOT NULL,
    text_lt TEXT NOT NULL,
    sequence INTEGER NOT NULL
);

CREATE INDEX ON terms_and_conditions_paragraph (terms_and_conditions_chapter_id);

CREATE TABLE faq_category (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    title_en TEXT NOT NULL,
    title_lt TEXT NOT NULL,
    sequence INTEGER NOT NULL
);

CREATE TABLE faq_question (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    category_id BIGINT NOT NULL REFERENCES faq_category (id),
    question_en TEXT NOT NULL,
    question_lt TEXT NOT NULL,
    answer_en TEXT NOT NULL,
    answer_lt TEXT NOT NULL,
    sequence INTEGER NOT NULL
);

CREATE INDEX ON faq_question (category_id);

CREATE TABLE transport_location (
    id VARCHAR(50) NOT NULL PRIMARY KEY,
    city_id BIGINT NOT NULL REFERENCES city (id),
    title TEXT NOT NULL,
    transport_type VARCHAR(20) NOT NULL CHECK (transport_type IN ('BUS', 'EXPRESS_BUS', 'NIGHT_BUS', 'TROLLEYBUS', 'MINIBUS', 'DISTRICT_BUS')),
    color VARCHAR(9) NOT NULL,
    latitude NUMERIC(8, 6) NOT NULL,
    longitude NUMERIC(8, 6) NOT NULL
);

CREATE INDEX ON transport_location (city_id);

CREATE TABLE data_import (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    date DATE NOT NULL,
    city_id BIGINT NOT NULL REFERENCES city (id),
    system TEXT NOT NULL,
    entity TEXT NOT NULL,
    action VARCHAR(20) NOT NULL CHECK (action IN ('CREATE', 'UPDATE', 'DELETE')),
    amount INTEGER NOT NULL
);

CREATE INDEX ON data_import (date);

CREATE TABLE data_import_result (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    date DATE NOT NULL,
    city_id BIGINT NOT NULL REFERENCES city (id),
    system TEXT NOT NULL,
    success BOOLEAN NOT NULL
);

CREATE INDEX ON data_import_result (date);

CREATE TABLE request (
    id BIGSERIAL NOT NULL PRIMARY KEY,
    time TIMESTAMP NOT NULL,
    method VARCHAR(10) NOT NULL CHECK (method IN ('GET', 'POST', 'PUT', 'DELETE')),
    path TEXT NOT NULL,
    parameters TEXT
);

CREATE SEQUENCE version;
