WITH city_data AS
    (SELECT * FROM (VALUES
        (1, 'Vilnius', 1, 54.572080, 25.016232, 54.843238, 25.548338, 'http://www.stops.lt/vilnius/vilnius/gtfs.zip', 'http://www.stops.lt/vilnius/gps_full.txt'),
        (2, 'Kaunas', 2, 54.777276, 23.754594, 54.983753, 24.108703, 'http://www.stops.lt/kaunas/kaunas/gtfs.zip', 'http://www.stops.lt/kaunas/gps_full.txt'),
        (3, 'Klaipėda', 3, 55.554587, 21.036702, 55.927052, 21.543558, 'http://www.stops.lt/klaipeda/klaipeda/gtfs.zip', 'http://www.stops.lt/klaipeda/gps_full.txt'),
        (4, 'Šiauliai', 4, 55.781651, 23.135575, 55.972323, 23.453209, 'http://www.stops.lt/siauliai/siauliai/gtfs.zip', NULL),
        (5, 'Panevėžys', 5, 55.669409, 24.231568, 55.801891, 24.475304, 'http://www.stops.lt/panevezys/panevezys/gtfs.zip', NULL)
    ) AS data)
INSERT INTO city (id, name, sequence, southwest_latitude, southwest_longitude, northeast_latitude, northeast_longitude, schedules_url, gps_url)
    SELECT * FROM city_data
        ON CONFLICT (id)
        DO UPDATE SET name = EXCLUDED.name, sequence = EXCLUDED.sequence, southwest_latitude = EXCLUDED.southwest_latitude, southwest_longitude = EXCLUDED.southwest_longitude, northeast_latitude = EXCLUDED.northeast_latitude, northeast_longitude = EXCLUDED.northeast_longitude, schedules_url = EXCLUDED.schedules_url, gps_url = EXCLUDED.gps_url;

WITH city_transport_type_data AS
    (SELECT * FROM (VALUES
        (1, 'BUS', '#FF0073A7'),
        (1, 'EXPRESS_BUS', '#FF128542'),
        (1, 'NIGHT_BUS', '#FF6D6E71'),
        (1, 'TROLLEYBUS', '#FFE42E2B'),
        (2, 'BUS', '#FFE31E17'),
        (2, 'TROLLEYBUS', '#FF4CB74A'),
        (3, 'BUS', '#FF0073AC'),
        (3, 'MINIBUS', '#FF008001'),
        (4, 'BUS', '#FF0C78BA'),
        (4, 'MINIBUS', '#FF0C78BA'),
        (5, 'BUS', '#FF0C78BA')
    ) AS data)
INSERT INTO city_transport_type (city_id, transport_type, color)
    SELECT * FROM city_transport_type_data
        ON CONFLICT (city_id, transport_type)
        DO UPDATE SET color = EXCLUDED.color;
