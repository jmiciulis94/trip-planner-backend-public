WITH terms_and_conditionsy_data AS
    (SELECT * FROM (VALUES
        (1, 1, '„Vėjas“ app Terms of Use', 'Naudojimosi programėle „Vėjas“ taisyklės', 'Last version: September 2nd, 2021', 'Paskutinė redakcija: 2021 m. rugsėjo 2 d.')
    ) AS data)
INSERT INTO terms_and_conditions (id, version, header_en, header_lt, footer_en, footer_lt)
    SELECT * FROM terms_and_conditionsy_data
        ON CONFLICT (id)
        DO UPDATE SET version = EXCLUDED.version, header_en = EXCLUDED.header_en, header_lt = EXCLUDED.header_lt, footer_en = EXCLUDED.footer_en, footer_lt = EXCLUDED.footer_lt;

WITH terms_and_conditions_chapter_data AS
    (SELECT * FROM (VALUES
        (1, 1, 'WHO ARE WE?', 'KAS MES ESAME?', 1),
        (2, 1, 'GENERAL PROVISIONS', 'BENDROSIOS NUOSTATOS', 2),
        (3, 1, 'APPLICATION MANAGEMENT', 'PROGRAMĖLĖS VALDYMAS', 3),
        (4, 1, 'RESPONSIBILITY', 'ATSAKOMYBĖ', 4)
    ) AS data)
INSERT INTO terms_and_conditions_chapter (id, terms_and_conditions_id, title_en, title_lt, sequence)
    SELECT * FROM terms_and_conditions_chapter_data
        ON CONFLICT (id)
        DO UPDATE SET terms_and_conditions_id = EXCLUDED.terms_and_conditions_id, title_en = EXCLUDED.title_en, title_lt = EXCLUDED.title_lt, sequence = EXCLUDED.sequence;

WITH terms_and_conditions_paragraph_data AS
    (SELECT * FROM (VALUES
        (1, 1, '1. The owner, administrator and manager of "Vėjas" app (hereinafter - Application) is MB "Vėjas". MB „Vėjas“ is registered at Skalvių st. 27, Kaunas, tel .: +370 663 77387, e-mail: info@vejas-app.lt, www.vejas-app.lt  (hereinafter - Company).', '1. „Vėjas“ programėlės (toliau – Programėlė) savininkė, administratorė ir valdytoja yra MB „Vėjas“. MB „Vėjas“ yra registruota Skalvių g. 27, Kaunas, tel.: +370 663 77387, el. paštas info@vejas-app.lt, www.vejas-app.lt (toliau – Įmonė).', 1),
        (2, 2, '2. The user of the Application confirms that he has read and agrees to these terms of use (hereinafter - Rules) and undertakes to comply with them. If the user does not agree with the Rules, he cannot use the Application.', '2. Programėlės naudotojas patvirtina, jog susipažino ir sutinka su šiomis naudojimosi taisyklėmis (toliau – Taisyklės) ir įsipareigoja jų laikytis. Jeigu naudotojas nesutinka su Taisyklėmis, jis negali naudotis Programėle.', 1),
        (3, 2, '3. The latest version of the Rules is published in the Application and on the website www.vejas-app.lt. The date of the last adjustment is indicated at the end of the Rules. The Rules may be changed unilaterally at any time taking into account the requirements established by legal acts, changes in the functionality of the software or other unforeseen circumstances that may affect the functioning of the Application by notifying about the changes in the Application and the website www.vejas-app.lt. Amendments to the Rules shall enter into force on the date of their publication. If the Application user continues to use the Application after the changes to the terms of use have been posted, he or she will be deemed to have agreed to all changes to the Rules.', '3. Taisyklių naujausia redakcija yra skelbiama Programėlėje ir interneto svetainėje www.vejas-app.lt. Paskutinės korekcijos data nurodyta taisyklių pabaigoje. Taisyklės gali būti bet kada vienašališkai keičiamos atsižvelgiant į teisės aktų nustatytus reikalavimus, programinės įrangos funkcionalumo pokyčius ar kitas nenumatytas aplinkybes, kurios gali turėti įtakos programėlės funkcionavimui, apie pakeitimus pranešant Programėlėje ir interneto svetainėje www.vejas-app.lt. Taisyklių pakeitimai įsigalioja nuo jų paskelbimo dienos. Jei Programėlės naudotojas po Taisyklių nuostatų pakeitimų paskelbimo ir toliau naudojasi programėle, laikoma, kad jis sutinka su visais Taisyklių pakeitimais.', 2),
        (4, 3, '4. The user of the Application may not adjust, distribute, copy the data contained in the Application. All rights to the Application, the Application logo, other Company trademarks and content contained in the Application are reserved. Any form of use of the Application for commercial purposes requires the consent of the Company.', '4. Programėlės naudotojas negali koreguoti, platinti, kopijuoti Programėlėje esančių duomenų. Visos teisės į Programėlę, Programėlės logotipą ir kitus Įmonės prekės ženklus, Programėlėje esantį turinį yra saugomi. Betkokia forma naudojant Programėlę komerciniais tikslais būtinas Įmonės sutikimas. ', 1),
        (5, 3, '5. The Company reserves the right, at its sole discretion, to temporarily or indefinitely prohibit the use of the Application.', '5. Įmonė savo nuožiūra be išankstinio įspėjimo turi teisę laikinai arba neribotam laikui uždrausti naudotis programėle.', 2),
        (6, 4, '6. The Company, its agents and partners shall not be liable for any damages that the user of the Application may suffer as a result of using the Application. The company is not responsible for the operation of the telephone equipment used, communication failures or other obstacles.', '6. Įmonė, jos atstovai ir partneriai nėra atsakingi už jokią žalą, kurią Programėlės naudotojas gali patirti dėl naudojimosi Programėle. Įmonė neprisiima atsakomybės dėl naudojamos telefoninės įrangos veikimo, ryšio sutrikimų ar kitų kliūčių.', 1),
        (7, 4, '7. The Application displays real-time public transport traffic data from publicly available sources and is not responsible for the reliability of this data.', '7. Programėlė atvaizduoja realaus laiko viešojo transporto judėjimo duomenis iš viešai prieinamų šaltinių ir nėra atsakinga už šių duomenų patikimumą.', 2)
    ) AS data)
INSERT INTO terms_and_conditions_paragraph (id, terms_and_conditions_chapter_id, text_en, text_lt, sequence)
    SELECT * FROM terms_and_conditions_paragraph_data
        ON CONFLICT (id)
        DO UPDATE SET terms_and_conditions_chapter_id = EXCLUDED.terms_and_conditions_chapter_id, text_en = EXCLUDED.text_en, text_lt = EXCLUDED.text_lt, sequence = EXCLUDED.sequence;
