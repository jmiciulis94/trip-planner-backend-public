package eu.sts.tripplanner.backend.schedulers;

import eu.sts.tripplanner.backend.entities.City;
import eu.sts.tripplanner.backend.entities.TransportLocation;
import eu.sts.tripplanner.backend.providers.merakas.converters.TransportLocationConverter;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasTransportLocation;
import eu.sts.tripplanner.backend.repositories.CityRepository;
import eu.sts.tripplanner.backend.services.TransportLocationService;
import eu.sts.tripplanner.backend.utils.FileUtils;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class GpsScheduler {

    private final CityRepository cityRepository;
    private final TransportLocationConverter transportLocationConverter;
    private final TransportLocationService transportLocationService;

    public GpsScheduler(CityRepository cityRepository, TransportLocationConverter transportLocationConverter, TransportLocationService transportLocationService) {
        this.cityRepository = cityRepository;
        this.transportLocationConverter = transportLocationConverter;
        this.transportLocationService = transportLocationService;
    }

    @Scheduled(fixedDelay = 4000)
    public void updateGps() throws IOException {
        List<City> cities = cityRepository.findAll();

        for (City city : cities) {
            if (city.getGpsUrl() != null) {
                List<MerakasTransportLocation> merakasTransportLocations = FileUtils.readFileFromUrl(city.getGpsUrl(), MerakasTransportLocation.class);
                List<TransportLocation> transportLocations = merakasTransportLocations.stream().map(mtl -> transportLocationConverter.convert(mtl, city)).collect(Collectors.toList());
                transportLocationService.updateTransportLocations(city, transportLocations);
            }
        }
    }
}
