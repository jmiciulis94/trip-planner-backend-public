package eu.sts.tripplanner.backend.schedulers;

import eu.sts.tripplanner.backend.entities.City;
import eu.sts.tripplanner.backend.entities.DataImportResult;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasCalendar;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasInfoWrapper;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasRoute;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasShape;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasStop;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasStopTime;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasTrip;
import eu.sts.tripplanner.backend.repositories.DataImportResultRepository;
import eu.sts.tripplanner.backend.services.CityService;
import eu.sts.tripplanner.backend.services.RouteService;
import eu.sts.tripplanner.backend.services.SchedulesService;
import eu.sts.tripplanner.backend.utils.FileUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import net.lingala.zip4j.ZipFile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SchedulesScheduler {

    public static final String SYSTEM_MERAKAS = "MERAKAS";

    private final CityService cityService;
    private final DataImportResultRepository dataImportResultRepository;
    private final RouteService routeService;
    private final SchedulesService schedulesService;

    public SchedulesScheduler(CityService cityService, DataImportResultRepository dataImportResultRepository, RouteService routeService, SchedulesService schedulesService) {
        this.cityService = cityService;
        this.dataImportResultRepository = dataImportResultRepository;
        this.routeService = routeService;
        this.schedulesService = schedulesService;
    }

    @Scheduled(fixedDelayString = "P1D")
    public void updateSchedules() throws IOException {
        log.info("Schedules scheduler started.");
        Long version = routeService.getNextVersionNumber();
        List<City> cities = cityService.getCities();

        for (City city : cities) {
            try {
                log.info(String.format("Downloading %s data.", city.getName()));
                File file = downloadZipFile(city.getSchedulesUrl());
                Path directory = unzipFiles(file);
                MerakasInfoWrapper merakasInfo = parseFiles(directory);
                schedulesService.storeInfo(merakasInfo, city.getId(), version);
            } catch (Exception ex) {
                storeImportInfo(city.getId(), false);
                continue;
            }

            storeImportInfo(city.getId(), true);
        }
        log.info("Schedules scheduler finished.");
    }

    private File downloadZipFile(String url) throws IOException {
        URL website = new URL(url);
        ReadableByteChannel readableByteChannel = Channels.newChannel(website.openStream());
        File tempFile = File.createTempFile("tpp", ".zip");
        FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
        fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);

        return tempFile;
    }

    private Path unzipFiles(File file) throws IOException {
        Path tempDirectory = Files.createTempDirectory(null);
        ZipFile zipFile = new ZipFile(file);
        zipFile.extractAll(tempDirectory.toString());

        return tempDirectory;
    }

    private MerakasInfoWrapper parseFiles(Path directory) throws FileNotFoundException {
        List<MerakasCalendar> calendars = FileUtils.readFileFromDisk(directory.resolve("calendar.txt").toAbsolutePath().toString(), MerakasCalendar.class);
        List<MerakasRoute> routes = FileUtils.readFileFromDisk(directory.resolve("routes.txt").toAbsolutePath().toString(), MerakasRoute.class);
        List<MerakasShape> shapes = FileUtils.readFileFromDisk(directory.resolve("shapes.txt").toAbsolutePath().toString(), MerakasShape.class);
        List<MerakasStopTime> stopTimes = FileUtils.readFileFromDisk(directory.resolve("stop_times.txt").toAbsolutePath().toString(), MerakasStopTime.class);
        List<MerakasStop> stops = FileUtils.readFileFromDisk(directory.resolve("stops.txt").toAbsolutePath().toString(), MerakasStop.class);
        List<MerakasTrip> trips = FileUtils.readFileFromDisk(directory.resolve("trips.txt").toAbsolutePath().toString(), MerakasTrip.class);

        return new MerakasInfoWrapper(calendars, routes, shapes, stopTimes, stops, trips);
    }

    private void storeImportInfo(Long cityId, Boolean success) {
        DataImportResult dataImportResult = new DataImportResult();
        dataImportResult.setDate(LocalDate.now());
        dataImportResult.setCityId(cityId);
        dataImportResult.setSystem(SchedulesScheduler.SYSTEM_MERAKAS);
        dataImportResult.setSuccess(success);

        dataImportResultRepository.save(dataImportResult);
    }
}
