package eu.sts.tripplanner.backend.providers.merakas.converters;

import eu.sts.tripplanner.backend.entities.Direction;
import eu.sts.tripplanner.backend.entities.DirectionStop;
import eu.sts.tripplanner.backend.entities.Stop;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasCalendar;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasShape;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasStopTime;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasTrip;
import eu.sts.tripplanner.backend.providers.merakas.utils.MerakasUtils;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Slf4j
@Component
public class DirectionConverter {

    private final DirectionStopConverter directionStopConverter;
    private final DirectionShapeConverter directionShapeConverter;

    public DirectionConverter(DirectionShapeConverter directionShapeConverter, DirectionStopConverter directionStopConverter) {
        this.directionShapeConverter = directionShapeConverter;
        this.directionStopConverter = directionStopConverter;
    }

    public Direction convert(List<MerakasTrip> merakasTrips, List<MerakasStopTime> merakasStopTimes, List<MerakasCalendar> merakasCalendars, List<MerakasShape> merakasShapes, Map<String, String> directionHeadSigns, List<Stop> stops) {
        Direction direction = new Direction();
        direction.setCode(merakasTrips.get(0).getShapeId());
        direction.setTitle(parseTitle(merakasTrips.get(0), directionHeadSigns));

        Map<String, MerakasCalendar> tripIdToCalendarMap = new HashMap<>();
        for (MerakasTrip merakasTrip : merakasTrips) {
            MerakasCalendar merakasCalendar = merakasCalendars.stream().filter(c -> c.getServiceId().equals(merakasTrip.getServiceId())).findFirst().orElse(null);
            tripIdToCalendarMap.put(merakasTrip.getTripId(), merakasCalendar);
        }

        List<MerakasShape> shapes = merakasShapes.stream().filter(s -> s.getId().equals(direction.getCode())).collect(Collectors.toList());

        direction.setDirectionStops(directionStopConverter.convert(merakasStopTimes, stops, tripIdToCalendarMap).stream().sorted(Comparator.comparing(DirectionStop::getSequence)).collect(Collectors.toList()));
        direction.setDirectionShapes(directionShapeConverter.convert(shapes));
        direction.setNumberOfTrips((long) merakasTrips.size());

        return direction;
    }

    private String parseTitle(MerakasTrip merakasTrip, Map<String, String> directionHeadSigns) {
        if (merakasTrip.getDirectionName() != null && merakasTrip.getDirectionName().trim().length() > 0) {
            return MerakasUtils.fixTitle(merakasTrip.getDirectionName());
        }

        int legs = StringUtils.countOccurrencesOf(merakasTrip.getShapeId(), "-") + 1;
        String endTitle = merakasTrip.getTripHeadsign();
        String[] tripLegCodes = merakasTrip.getShapeId().substring(merakasTrip.getRouteId().length() + 1).split("-");

        if (legs == 2) {
            String startTitle = getTitle(tripLegCodes[0], directionHeadSigns, merakasTrip);
            return startTitle + " - " + endTitle;
        } else if (legs == 3) {
            String startTitle = getTitle(tripLegCodes[0], directionHeadSigns, merakasTrip);
            String middleTitle = getTitle(tripLegCodes[1], directionHeadSigns, merakasTrip);
            return startTitle + " - " + middleTitle + " - " + endTitle;
        } else {
            throw new RuntimeException("Cannot parse direction title. ShapeId=" + merakasTrip.getShapeId());
        }
    }

    private String getTitle(String code, Map<String, String> directionHeadSigns, MerakasTrip merakasTrip) {
        String title = directionHeadSigns.get(code);
        if (title == null) {
            log.warn(String.format("Can not parse correct title for direction '%s'", merakasTrip.getShapeId()));
            return "Unknown";
        } else {
            return title;
        }
    }
}
