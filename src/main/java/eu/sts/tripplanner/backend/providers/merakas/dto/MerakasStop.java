package eu.sts.tripplanner.backend.providers.merakas.dto;

import com.opencsv.bean.CsvBindByName;
import java.math.BigDecimal;
import lombok.Data;

@Data
public class MerakasStop {

    @CsvBindByName(column = "stop_id")
    private Long id;

    @CsvBindByName(column = "stop_name")
    private String name;

    @CsvBindByName(column = "stop_lat")
    private BigDecimal latitude;

    @CsvBindByName(column = "stop_lon")
    private BigDecimal longitude;
}
