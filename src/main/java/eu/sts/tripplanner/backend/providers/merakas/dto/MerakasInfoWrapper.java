package eu.sts.tripplanner.backend.providers.merakas.dto;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MerakasInfoWrapper {

    private List<MerakasCalendar> calendars;
    private List<MerakasRoute> routes;
    private List<MerakasShape> shapes;
    private List<MerakasStopTime> stopTimes;
    private List<MerakasStop> stops;
    private List<MerakasTrip> trips;
}
