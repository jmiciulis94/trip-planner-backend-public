package eu.sts.tripplanner.backend.providers.merakas.utils;

public class MerakasUtils {

    private MerakasUtils() {
    }

    public static String fixTitle(String title) {
        return title.replace("-", " - ").replace("–", " - ").replace("  ", " ");
    }
}
