package eu.sts.tripplanner.backend.providers.merakas.dto;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;

@Data
public class MerakasTransportLocation {

    @CsvBindByName(column = "MasinosNumeris")
    private String id;

    @CsvBindByName(column = "Transportas")
    private String transportType;

    @CsvBindByName(column = "Marsrutas")
    private String title;

    @CsvBindByName(column = "Platuma")
    private String latitude;

    @CsvBindByName(column = "Ilguma")
    private String longitude;
}
