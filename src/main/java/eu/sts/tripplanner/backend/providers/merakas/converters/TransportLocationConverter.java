package eu.sts.tripplanner.backend.providers.merakas.converters;

import eu.sts.tripplanner.backend.constants.TransportType;
import eu.sts.tripplanner.backend.entities.City;
import eu.sts.tripplanner.backend.entities.CityTransportType;
import eu.sts.tripplanner.backend.entities.TransportLocation;
import eu.sts.tripplanner.backend.exceptions.SchedulesParsingException;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasTransportLocation;
import java.math.BigDecimal;
import org.springframework.stereotype.Component;

@Component
public class TransportLocationConverter {

    private static final long VILNIUS_CITY_ID = 1;

    public TransportLocation convert(MerakasTransportLocation merakasTransportLocation, City city) {
        TransportType transportType = parseTransportType(city.getId(), merakasTransportLocation);
        CityTransportType cityTransportType = city.getTransportTypes().stream().filter(tt -> tt.getTransportType().equals(transportType)).findFirst().orElse(null);

        TransportLocation transportLocation = new TransportLocation();
        transportLocation.setId(merakasTransportLocation.getId());
        transportLocation.setCityId(city.getId());
        transportLocation.setTitle(merakasTransportLocation.getTitle());
        transportLocation.setTransportType(transportType);
        transportLocation.setColor(cityTransportType != null ? cityTransportType.getColor() : null);
        transportLocation.setLatitude(parseCoordinates(merakasTransportLocation.getLatitude()));
        transportLocation.setLongitude(parseCoordinates(merakasTransportLocation.getLongitude()));

        return transportLocation;
    }

    private TransportType parseTransportType(Long cityId, MerakasTransportLocation merakasTransportLocation) {

        if (cityId == VILNIUS_CITY_ID && merakasTransportLocation.getTransportType().equals("Autobusai") && merakasTransportLocation.getTitle().endsWith("G")) {
            return TransportType.EXPRESS_BUS;
        } else if (cityId == VILNIUS_CITY_ID && merakasTransportLocation.getTransportType().equals("Autobusai") && merakasTransportLocation.getTitle().endsWith("N")) {
            return TransportType.NIGHT_BUS;
        } else if (merakasTransportLocation.getTransportType().equals("Autobusai")) {
            return TransportType.BUS;
        } else if (merakasTransportLocation.getTransportType().equals("Troleibusai")) {
            return TransportType.TROLLEYBUS;
        }

        throw new SchedulesParsingException("Cannot find parse TransportType. Was trying to parse=" + merakasTransportLocation.getTransportType());
    }

    private BigDecimal parseCoordinates(String coordinate) {

        String tempCoordinate = coordinate.substring(0, 2) + "." + coordinate.substring(2);

        return new BigDecimal(tempCoordinate);
    }
}
