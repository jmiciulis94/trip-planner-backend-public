package eu.sts.tripplanner.backend.providers.merakas.converters;

import eu.sts.tripplanner.backend.entities.StopTime;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasCalendar;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasStopTime;
import java.time.LocalTime;
import org.springframework.stereotype.Component;

@Component
public class StopTimeConverter {

    public StopTime convert(MerakasStopTime merakasStopTime, MerakasCalendar merakasCalendar) {
        StopTime stopTime = new StopTime();
        stopTime.setTripCode(String.valueOf(merakasStopTime.getTripId()));
        stopTime.setMonday(merakasCalendar.getMonday() == 1);
        stopTime.setTuesday(merakasCalendar.getTuesday() == 1);
        stopTime.setWednesday(merakasCalendar.getWednesday() == 1);
        stopTime.setThursday(merakasCalendar.getThursday() == 1);
        stopTime.setFriday(merakasCalendar.getFriday() == 1);
        stopTime.setSaturday(merakasCalendar.getSaturday() == 1);
        stopTime.setSunday(merakasCalendar.getSunday() == 1);
        stopTime.setTime(LocalTime.parse(fixInvalidHour(merakasStopTime.getStopSequence().compareTo(1L) == 0 ? merakasStopTime.getDepartureTime() : merakasStopTime.getArrivalTime())));

        return stopTime;
    }

    private String fixInvalidHour(String time) {
        if (time.startsWith("24")) {
            return "00" + time.substring(2);
        } else if (time.startsWith("25")) {
            return "01" + time.substring(2);
        } else if (time.startsWith("26")) {
            return "02" + time.substring(2);
        } else if (time.startsWith("27")) {
            return "03" + time.substring(2);
        } else if (time.startsWith("28")) {
            return "04" + time.substring(2);
        } else if (time.startsWith("29")) {
            return "05" + time.substring(2);
        } else if (time.startsWith("30")) {
            return "06" + time.substring(2);
        } else if (time.startsWith("31")) {
            return "07" + time.substring(2);
        } else if (time.startsWith("32")) {
            return "08" + time.substring(2);
        } else {
            return time;
        }
    }
}
