package eu.sts.tripplanner.backend.providers.merakas.dto;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;

@Data
public class MerakasStopTime {

    @CsvBindByName(column = "trip_id")
    private String tripId;

    @CsvBindByName(column = "arrival_time")
    private String arrivalTime;

    @CsvBindByName(column = "departure_time")
    private String departureTime;

    @CsvBindByName(column = "stop_id")
    private Long stopId;

    @CsvBindByName(column = "stop_sequence")
    private Long stopSequence;
}
