package eu.sts.tripplanner.backend.providers.merakas.dto;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;

@Data
public class MerakasTrip {

    @CsvBindByName(column = "route_id")
    private String routeId;

    @CsvBindByName(column = "service_id")
    private Long serviceId;

    @CsvBindByName(column = "trip_id")
    private String tripId;

    @CsvBindByName(column = "trip_headsign")
    private String tripHeadsign;

    @CsvBindByName(column = "shape_id")
    private String shapeId;

    @CsvBindByName(column = "direction_name")
    private String directionName;
}
