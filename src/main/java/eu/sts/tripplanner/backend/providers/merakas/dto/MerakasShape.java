package eu.sts.tripplanner.backend.providers.merakas.dto;

import com.opencsv.bean.CsvBindByName;
import java.math.BigDecimal;
import lombok.Data;

@Data
public class MerakasShape {

    @CsvBindByName(column = "shape_id")
    private String id;

    @CsvBindByName(column = "shape_pt_lat")
    private BigDecimal latitude;

    @CsvBindByName(column = "shape_pt_lon")
    private BigDecimal longitude;

    @CsvBindByName(column = "shape_pt_sequence")
    private Long sequence;
}
