package eu.sts.tripplanner.backend.providers.merakas.converters;

import eu.sts.tripplanner.backend.constants.TransportType;
import eu.sts.tripplanner.backend.entities.Direction;
import eu.sts.tripplanner.backend.entities.Route;
import eu.sts.tripplanner.backend.entities.Stop;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasCalendar;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasRoute;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasShape;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasStopTime;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasTrip;
import eu.sts.tripplanner.backend.providers.merakas.utils.MerakasUtils;
import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class RouteConverter {

    private static final int VALIDITY_TEXT_LENGTH = 13;
    private static final Pattern VALIDITY_PATTERN = Pattern.compile("Nuo(\\d+).(\\d+)d.");
    private static final String BUS_INDICATOR = "_bus_";
    private static final String EXPRESS_BUS_INDICATOR = "_expressbus_";
    private static final String NIGHT_BUS_INDICATOR = "_nightbus_";
    private static final String TROLLEYBUS_INDICATOR = "_trol_";
    private static final String MINIBUS_INDICATOR = "_minibus_";
    private static final String DISTRICT_BUS_INDICATOR = "_intercitybus_";
    private static final String SUBURBAN_BUS_INDICATOR = "_suburbanbus_";

    private final DirectionConverter directionConverter;

    public RouteConverter(DirectionConverter directionConverter) {
        this.directionConverter = directionConverter;
    }

    public Route convert(MerakasRoute merakasRoute, List<MerakasTrip> merakasTrips, List<MerakasStopTime> merakasStopTimes, List<MerakasCalendar> merakasCalendars, List<MerakasShape> merakasShapes, List<Stop> stops, Long cityId) {
        Map<String, String> directionHeadSigns = collectDirectionHeadSigns(merakasTrips);
        Map<String, List<MerakasTrip>> shapeIdToTripsMap = merakasTrips.stream().collect(Collectors.groupingBy(MerakasTrip::getShapeId));

        List<Direction> directions = new ArrayList<>();
        for (Map.Entry<String, List<MerakasTrip>> entry : shapeIdToTripsMap.entrySet()) {
            List<MerakasTrip> trips = entry.getValue();
            List<String> tripIds = trips.stream().map(MerakasTrip::getTripId).collect(Collectors.toList());
            List<MerakasStopTime> stopTimes = merakasStopTimes.stream().filter(st -> tripIds.contains(st.getTripId())).collect(Collectors.toList());
            Direction direction = directionConverter.convert(trips, stopTimes, merakasCalendars, merakasShapes, directionHeadSigns, stops);
            directions.add(direction);
        }

        directions = directions.stream().sorted(Comparator.comparing(Direction::getNumberOfTrips).reversed()).collect(Collectors.toList());
        for (int i = 0; i < directions.size(); i++) {
            Direction direction = directions.get(i);
            direction.setSequence((long) i + 1);
        }

        Route route = new Route();
        route.setCode(merakasRoute.getId());
        route.setCityId(cityId);
        route.setTransportType(parseTransportType(merakasRoute.getId()));
        route.setNumber(merakasRoute.getShortName());
        route.setTitle(MerakasUtils.fixTitle(merakasRoute.getLongName()));
        route.setDirections(directions);

        parseValidationPeriodAndFixRouteNumber(route);

        return route;
    }

    private void parseValidationPeriodAndFixRouteNumber(Route route) {
        Matcher matcher = VALIDITY_PATTERN.matcher(route.getCode());
        boolean haveValidityDates = matcher.find();
        if (haveValidityDates) {
            int month = Integer.parseInt(matcher.group(1));
            int day = Integer.parseInt(matcher.group(2));

            LocalDate currentDate = LocalDate.now();
            LocalDate validSince = LocalDate.of(Year.now().getValue(), month, day);
            if (validSince.isBefore(currentDate)) {
                validSince = validSince.plusYears(1);
            }

            route.setValidSince(validSince);
            route.setNumber(route.getNumber().substring(0, route.getNumber().length() - VALIDITY_TEXT_LENGTH));
        }
    }

    private Map<String, String> collectDirectionHeadSigns(List<MerakasTrip> merakasTrips) {
        Map<String, String> directionHeadSigns = new HashMap<>();
        for (MerakasTrip merakasTrip : merakasTrips) {
            String key = merakasTrip.getShapeId().substring(merakasTrip.getShapeId().lastIndexOf("-") + 1);
            String value = merakasTrip.getTripHeadsign();
            directionHeadSigns.put(key, value);
        }

        return directionHeadSigns;
    }

    private TransportType parseTransportType(String routeId) {
        if (routeId.contains(BUS_INDICATOR)) {
            return TransportType.BUS;
        } else if (routeId.contains(EXPRESS_BUS_INDICATOR)) {
            return TransportType.EXPRESS_BUS;
        } else if (routeId.contains(NIGHT_BUS_INDICATOR)) {
            return TransportType.NIGHT_BUS;
        } else if (routeId.contains(TROLLEYBUS_INDICATOR)) {
            return TransportType.TROLLEYBUS;
        } else if (routeId.contains(MINIBUS_INDICATOR)) {
            return TransportType.MINIBUS;
        } else if (routeId.contains(DISTRICT_BUS_INDICATOR) || routeId.contains(SUBURBAN_BUS_INDICATOR)) {
            return TransportType.DISTRICT_BUS;
        } else {
            throw new RuntimeException("Cannot parse transport type. RouteId=" + routeId);
        }
    }
}
