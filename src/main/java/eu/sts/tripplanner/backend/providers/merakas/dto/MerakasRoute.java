package eu.sts.tripplanner.backend.providers.merakas.dto;

import com.opencsv.bean.CsvBindByName;
import lombok.Data;

@Data
public class MerakasRoute {

    @CsvBindByName(column = "route_id")
    private String id;

    @CsvBindByName(column = "route_short_name")
    private String shortName;

    @CsvBindByName(column = "route_long_name")
    private String longName;
}
