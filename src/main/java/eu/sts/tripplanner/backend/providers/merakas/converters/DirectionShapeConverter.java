package eu.sts.tripplanner.backend.providers.merakas.converters;

import eu.sts.tripplanner.backend.entities.DirectionShape;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasShape;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class DirectionShapeConverter {

    public List<DirectionShape> convert(List<MerakasShape> merakasShapes) {
        List<DirectionShape> directionShapes = new ArrayList<>();

        for (MerakasShape merakasShape : merakasShapes) {
            DirectionShape directionShape = new DirectionShape();
            directionShape.setLatitude(merakasShape.getLatitude());
            directionShape.setLongitude(merakasShape.getLongitude());
            directionShape.setSequence(merakasShape.getSequence());
            directionShapes.add(directionShape);
        }

        return directionShapes;
    }
}
