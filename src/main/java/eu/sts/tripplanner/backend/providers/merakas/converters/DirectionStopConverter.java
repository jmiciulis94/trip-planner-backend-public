package eu.sts.tripplanner.backend.providers.merakas.converters;

import eu.sts.tripplanner.backend.entities.DirectionStop;
import eu.sts.tripplanner.backend.entities.Stop;
import eu.sts.tripplanner.backend.entities.StopTime;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasCalendar;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasStopTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class DirectionStopConverter {

    private final StopTimeConverter stopTimeConverter;

    public DirectionStopConverter(StopTimeConverter stopTimeConverter) {
        this.stopTimeConverter = stopTimeConverter;
    }

    public List<DirectionStop> convert(List<MerakasStopTime> merakasStopTimes, List<Stop> stops, Map<String, MerakasCalendar> tripIdToCalendarMap) {
        List<DirectionStop> directionStops = new ArrayList<>();

        for (MerakasStopTime merakasStopTime : merakasStopTimes) {
            DirectionStop directionStop = directionStops.stream().filter(ds -> ds.getStop().getCode().equals(merakasStopTime.getStopId())).findFirst().orElse(null);

            if (directionStop == null) {
                directionStop = new DirectionStop();
                directionStop.setStop(stops.stream().filter(s -> s.getCode().equals(merakasStopTime.getStopId())).findFirst().orElse(null));
                directionStop.setSequence(merakasStopTime.getStopSequence());
                directionStops.add(directionStop);
            }

            StopTime stopTime = stopTimeConverter.convert(merakasStopTime, tripIdToCalendarMap.get(merakasStopTime.getTripId()));
            directionStop.getStopTimes().add(stopTime);
        }

        directionStops.forEach(ds -> ds.setStopTimes(ds.getStopTimes().stream().sorted(Comparator.comparing(StopTime::getTime).thenComparing(StopTime::getTripCode)).collect(Collectors.toList())));

        return directionStops;
    }
}
