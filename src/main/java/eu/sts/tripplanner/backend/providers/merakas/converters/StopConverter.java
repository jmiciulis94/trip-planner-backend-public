package eu.sts.tripplanner.backend.providers.merakas.converters;

import eu.sts.tripplanner.backend.entities.Stop;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasStop;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class StopConverter {

    public Stop convert(MerakasStop merakasStop, List<Stop> existingStops, Long cityId) {
        Stop existing = existingStops.stream().filter(s -> s.getCode().equals(merakasStop.getId())).findFirst().orElse(null);

        Stop stop = new Stop();
        stop.setId(existing != null ? existing.getId() : null);
        stop.setCode(merakasStop.getId());
        stop.setCityId(cityId);
        stop.setTitle(merakasStop.getName());
        stop.setLatitude(merakasStop.getLatitude());
        stop.setLongitude(merakasStop.getLongitude());

        return stop;
    }
}
