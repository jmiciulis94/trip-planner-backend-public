package eu.sts.tripplanner.backend.providers.merakas.dto;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;
import lombok.Data;

@Data
public class MerakasCalendar {

    @CsvBindByName(column = "service_id")
    private Long serviceId;

    @CsvBindByName(column = "monday")
    private Integer monday;

    @CsvBindByName(column = "tuesday")
    private Integer tuesday;

    @CsvBindByName(column = "wednesday")
    @CsvBindByPosition(position = 3)
    private Integer wednesday;

    @CsvBindByName(column = "thursday")
    @CsvBindByPosition(position = 4)
    private Integer thursday;

    @CsvBindByName(column = "friday")
    @CsvBindByPosition(position = 5)
    private Integer friday;

    @CsvBindByName(column = "saturday")
    @CsvBindByPosition(position = 6)
    private Integer saturday;

    @CsvBindByName(column = "sunday")
    @CsvBindByPosition(position = 7)
    private Integer sunday;
}
