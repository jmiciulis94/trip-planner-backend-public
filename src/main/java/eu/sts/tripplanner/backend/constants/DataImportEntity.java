package eu.sts.tripplanner.backend.constants;

public enum DataImportEntity {

    STOP, ROUTE
}
