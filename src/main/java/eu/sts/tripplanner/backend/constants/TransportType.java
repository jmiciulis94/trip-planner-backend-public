package eu.sts.tripplanner.backend.constants;

public enum TransportType {

    BUS, EXPRESS_BUS, NIGHT_BUS, TROLLEYBUS, MINIBUS, DISTRICT_BUS
}
