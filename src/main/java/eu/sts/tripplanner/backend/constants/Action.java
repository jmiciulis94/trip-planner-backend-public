package eu.sts.tripplanner.backend.constants;

public enum Action {

    CREATE, UPDATE, DELETE
}
