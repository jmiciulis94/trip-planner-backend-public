package eu.sts.tripplanner.backend.utils;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.List;
import org.apache.commons.io.input.BOMInputStream;

public class FileUtils {

    private FileUtils() {
    }

    public static <T> List<T> readFileFromUrl(String path, Class<T> clazz) throws IOException {
        URL url = new URL(path);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
        return readFile(bufferedReader, clazz);
    }

    public static <T> List<T> readFileFromDisk(String path, Class<T> clazz) throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(path);
        InputStreamReader inputStreamReader = new InputStreamReader(new BOMInputStream(fileInputStream), StandardCharsets.UTF_8);
        return readFile(inputStreamReader, clazz);
    }

    private static <T> List<T> readFile(Reader inputReader, Class<T> clazz) {
        Reader reader = new BufferedReader(inputReader);
        HeaderColumnNameMappingStrategy<T> strategy = new HeaderColumnNameMappingStrategy<>();
        strategy.setType(clazz);
        CsvToBean<T> csvParser = new CsvToBeanBuilder<T>(reader).withType(clazz).withMappingStrategy(strategy).withIgnoreLeadingWhiteSpace(true).withSeparator(',').build();
        return csvParser.parse();
    }
}
