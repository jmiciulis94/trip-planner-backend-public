package eu.sts.tripplanner.backend.controllers;

import eu.sts.tripplanner.backend.Application;
import eu.sts.tripplanner.backend.constants.TransportType;
import eu.sts.tripplanner.backend.dto.TransportLocationDto;
import eu.sts.tripplanner.backend.services.TransportLocationService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(Application.PATH + "/gps")
@CrossOrigin
public class TransportLocationController {

    private final TransportLocationService transportLocationService;

    public TransportLocationController(TransportLocationService transportLocationService) {
        this.transportLocationService = transportLocationService;
    }

    @GetMapping
    public List<TransportLocationDto> getTransportLocations(@RequestParam("cityId") Long cityId, @RequestParam(value = "transportTypes", required = false) List<TransportType> transportTypes) {
        log.info(String.format("/api/gps?cityId=%d", cityId));
        if (transportTypes != null && transportTypes.size() > 0) {
            return transportLocationService.getTransportLocations(cityId, transportTypes);
        } else {
            return transportLocationService.getTransportLocations(cityId);
        }
    }
}
