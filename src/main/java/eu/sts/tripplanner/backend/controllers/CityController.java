package eu.sts.tripplanner.backend.controllers;

import eu.sts.tripplanner.backend.Application;
import eu.sts.tripplanner.backend.dto.CityDto;
import eu.sts.tripplanner.backend.services.CityService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(Application.PATH + "/city")
@CrossOrigin
public class CityController {

    private final CityService cityService;

    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @GetMapping
    public List<CityDto> getCities() {
        log.info("/api/city");
        return cityService.getCitiesDto();
    }
}
