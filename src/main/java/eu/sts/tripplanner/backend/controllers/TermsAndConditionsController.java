package eu.sts.tripplanner.backend.controllers;

import eu.sts.tripplanner.backend.Application;
import eu.sts.tripplanner.backend.entities.TermsAndConditions;
import eu.sts.tripplanner.backend.services.TermsAndConditionsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(Application.PATH + "/terms-and-conditions")
@CrossOrigin
public class TermsAndConditionsController {

    private final TermsAndConditionsService termsAndConditionsService;

    public TermsAndConditionsController(TermsAndConditionsService termsAndConditionsService) {
        this.termsAndConditionsService = termsAndConditionsService;
    }

    @GetMapping
    public TermsAndConditions getTermsAndConditions() {
        log.info("/api/terms-and-conditions");
        return termsAndConditionsService.getTermsAndConditions();
    }
}
