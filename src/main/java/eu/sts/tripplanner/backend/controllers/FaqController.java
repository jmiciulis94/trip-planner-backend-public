package eu.sts.tripplanner.backend.controllers;

import eu.sts.tripplanner.backend.Application;
import eu.sts.tripplanner.backend.entities.FaqCategory;
import eu.sts.tripplanner.backend.services.FaqService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(Application.PATH + "/faq")
@CrossOrigin
public class FaqController {

    private final FaqService faqService;

    public FaqController(FaqService faqService) {
        this.faqService = faqService;
    }

    @GetMapping
    public List<FaqCategory> getCategories() {
        log.info("/api/faq");
        return faqService.getCategories();
    }
}
