package eu.sts.tripplanner.backend.controllers;

import eu.sts.tripplanner.backend.Application;
import eu.sts.tripplanner.backend.dto.DirectionDto;
import eu.sts.tripplanner.backend.dto.RouteDto;
import eu.sts.tripplanner.backend.dto.StopTimeDto;
import eu.sts.tripplanner.backend.services.RouteService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(Application.PATH + "/route")
@CrossOrigin
public class RouteController {

    private final RouteService routeService;

    public RouteController(RouteService routeService) {
        this.routeService = routeService;
    }

    @GetMapping
    public List<RouteDto> getRoutes(@RequestParam("cityId") Long cityId) {
        log.info(String.format("/api/route?cityId=%d", cityId));
        return routeService.getRoutesDto(cityId);
    }

    @GetMapping("/{routeId}/directions")
    public List<DirectionDto> getDirections(@PathVariable Long routeId) {
        log.info(String.format("/api/route/%d/directions", routeId));
        return routeService.getDirections(routeId);
    }

    @GetMapping("/{routeId}/stop-times")
    public List<StopTimeDto> getStopTimes(@PathVariable Long routeId) {
        log.info(String.format("/api/route/%d/stop-times", routeId));
        return routeService.getStopTimes(routeId);
    }
}
