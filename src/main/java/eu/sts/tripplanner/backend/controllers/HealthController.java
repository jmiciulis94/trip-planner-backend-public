package eu.sts.tripplanner.backend.controllers;

import eu.sts.tripplanner.backend.Application;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(Application.PATH + "/health")
@CrossOrigin
public class HealthController {

    @GetMapping
    public String getCategories() {
        log.info("/api/health");
        return "OK.";
    }
}
