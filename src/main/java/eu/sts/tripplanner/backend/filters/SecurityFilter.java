package eu.sts.tripplanner.backend.filters;

import eu.sts.tripplanner.backend.Application;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class SecurityFilter implements Filter {

    private static final String SECURITY_HEADER = "secret-code";
    private static final String SECURITY_HEADER_VALUE = "51oL$hbvP64Rjh6^%df!";
    private static final String HEALTH_CHECK_ENDPOINT = "/" + Application.PATH + "/health";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String code = httpServletRequest.getHeader(SECURITY_HEADER);
        String url = httpServletRequest.getRequestURI();

        if (!url.equals(HEALTH_CHECK_ENDPOINT) && (code == null || !code.equals(SECURITY_HEADER_VALUE))) {
            HttpServletResponse httpServletResponse = (HttpServletResponse) response;
            httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
        } else {
            chain.doFilter(request, response);
        }
    }
}
