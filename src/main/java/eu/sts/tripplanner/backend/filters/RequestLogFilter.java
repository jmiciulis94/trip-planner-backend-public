package eu.sts.tripplanner.backend.filters;

import eu.sts.tripplanner.backend.entities.Request;
import eu.sts.tripplanner.backend.repositories.RequestRepository;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(2)
public class RequestLogFilter implements Filter {

    private final RequestRepository requestRepository;

    public RequestLogFilter(RequestRepository requestRepository) {
        this.requestRepository = requestRepository;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String method = httpServletRequest.getMethod();
        String url = httpServletRequest.getRequestURI();
        String parameters = httpServletRequest.getQueryString();

        Request requestInfo = new Request();
        requestInfo.setTime(LocalDateTime.now(ZoneOffset.UTC));
        requestInfo.setMethod(method);
        requestInfo.setPath(url);
        requestInfo.setParameters(parameters);

        requestRepository.save(requestInfo);

        chain.doFilter(request, response);
    }
}
