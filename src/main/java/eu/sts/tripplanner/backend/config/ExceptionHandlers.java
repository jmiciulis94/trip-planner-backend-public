package eu.sts.tripplanner.backend.config;

import eu.sts.tripplanner.backend.exceptions.ServerError;
import eu.sts.tripplanner.backend.exceptions.ValidationException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@ControllerAdvice
public class ExceptionHandlers {

    private final MessageSource messageSource;

    public ExceptionHandlers(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(ValidationException.class)
    @ResponseBody
    public ServerError handleValidationException(HttpServletResponse response, ValidationException ex) {
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);

        String header = ex.getErrors().getGlobalError() != null ? messageSource.getMessage(ex.getErrors().getGlobalError(), Locale.getDefault()) : "";
        List<String> items = ex.getErrors().getFieldErrors().stream().map(e -> messageSource.getMessage(e, Locale.getDefault())).collect(Collectors.toList());

        return new ServerError(header, items);
    }

    @ExceptionHandler(value = IllegalArgumentException.class)
    @ResponseBody
    public ServerError handleIllegalArgumentException(HttpServletResponse response, IllegalArgumentException ex) {
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        String header = ex.getMessage();
        return new ServerError(header, Collections.emptyList());
    }

    @ExceptionHandler(value = SecurityException.class)
    @ResponseBody
    public ServerError handleSecurityException(HttpServletResponse response) {
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        String header = messageSource.getMessage("unauthorizedResource", null, Locale.getDefault());
        return new ServerError(header, Collections.emptyList());
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ServerError handleGeneralException(HttpServletResponse response, Exception ex) {
        log.error(ex.getMessage(), ex);

        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        String header = messageSource.getMessage("serverError", null, Locale.getDefault());
        return new ServerError(header, new ArrayList<>());
    }
}
