package eu.sts.tripplanner.backend.services;

import eu.sts.tripplanner.backend.constants.Action;
import eu.sts.tripplanner.backend.constants.DataImportEntity;
import eu.sts.tripplanner.backend.entities.DataImport;
import eu.sts.tripplanner.backend.entities.Direction;
import eu.sts.tripplanner.backend.entities.DirectionShape;
import eu.sts.tripplanner.backend.entities.DirectionStop;
import eu.sts.tripplanner.backend.entities.Route;
import eu.sts.tripplanner.backend.entities.Stop;
import eu.sts.tripplanner.backend.entities.StopTime;
import eu.sts.tripplanner.backend.providers.merakas.converters.RouteConverter;
import eu.sts.tripplanner.backend.providers.merakas.converters.StopConverter;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasInfoWrapper;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasRoute;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasStop;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasStopTime;
import eu.sts.tripplanner.backend.providers.merakas.dto.MerakasTrip;
import eu.sts.tripplanner.backend.repositories.DataImportRepository;
import eu.sts.tripplanner.backend.schedulers.SchedulesScheduler;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SchedulesService {

    private static final List<String> ILLEGAL_ROUTE_NAMES = List.of("Ekstremaliossituacijosmetunevaziuoja", "Karantinometunevaziuoja");

    private final StopService stopService;
    private final RouteService routeService;
    private final DataImportRepository dataImportRepository;
    private final StopConverter stopConverter;
    private final RouteConverter routeConverter;

    public SchedulesService(StopService stopService, RouteService routeService, DataImportRepository dataImportRepository, StopConverter stopConverter, RouteConverter routeConverter) {
        this.stopService = stopService;
        this.routeService = routeService;
        this.dataImportRepository = dataImportRepository;
        this.stopConverter = stopConverter;
        this.routeConverter = routeConverter;
    }

    public void storeInfo(MerakasInfoWrapper merakasInfo, Long cityId, Long version) {
        List<Route> currentRoutes = routeService.getRoutes(cityId);
        List<Stop> currentStops = stopService.getStops(cityId);

        storeStops(merakasInfo.getStops(), currentStops, cityId, version);
        storeRoutes(merakasInfo, currentRoutes, cityId, version);
        deleteRoutes(merakasInfo.getRoutes(), currentRoutes, cityId);
        deleteStops(merakasInfo.getStops(), currentStops, cityId);
    }

    private void deleteStops(List<MerakasStop> merakasStops, List<Stop> currentStops, Long cityId) {
        List<Long> merakasStopsCodes = merakasStops.stream().map(MerakasStop::getId).collect(Collectors.toList());
        List<Stop> stopsToDelete = currentStops.stream().filter(s -> !merakasStopsCodes.contains(s.getCode())).collect(Collectors.toList());

        log.info(String.format("Deleting %d stops.", stopsToDelete.size()));
        for (Stop stop : stopsToDelete) {
            log.info(String.format("Deleting stop '%s'.", stop.getTitle()));
            stopService.delete(stop.getId());
        }

        if (!stopsToDelete.isEmpty()) {
            storeImportInfo(cityId, DataImportEntity.STOP, Action.DELETE, (long) stopsToDelete.size());
        }
    }

    private void storeStops(List<MerakasStop> merakasStops, List<Stop> currentStops, Long cityId, Long version) {
        List<Stop> stopsToStore = new ArrayList<>();

        for (MerakasStop merakasStop : merakasStops) {
            Stop stop = stopConverter.convert(merakasStop, currentStops, cityId);
            Stop currentStop = currentStops.stream().filter(s -> s.getCode().equals(stop.getCode())).findFirst().orElse(null);
            if (currentStop == null || haveChanges(currentStop, stop)) {
                stopsToStore.add(stop);
            }
        }

        log.info(String.format("Storing %d stops.", stopsToStore.size()));

        long stopsCreated = 0;
        long stopsUpdated = 0;
        for (Stop stop : stopsToStore) {
            log.info(String.format("Storing stop '%s'.", stop.getTitle()));
            currentStops.stream().filter(s -> s.getCode().equals(stop.getCode())).findFirst().ifPresent(s -> stop.setId(s.getId()));
            stop.setVersion(version);
            stopService.save(stop);

            boolean exists = currentStops.stream().anyMatch(s -> s.getCode().equals(stop.getCode()));
            if (exists) {
                stopsUpdated++;
            } else {
                stopsCreated++;
            }
        }

        if (stopsCreated > 0) {
            storeImportInfo(cityId, DataImportEntity.STOP, Action.CREATE, stopsCreated);
        }
        if (stopsUpdated > 0) {
            storeImportInfo(cityId, DataImportEntity.STOP, Action.UPDATE, stopsUpdated);
        }

        log.info("Stops stored.");
    }

    private void deleteRoutes(List<MerakasRoute> routes, List<Route> currentRoutes, Long cityId) {
        List<String> merakasRoutesCodes = routes.stream().map(MerakasRoute::getId).collect(Collectors.toList());
        List<Route> routesToDelete = currentRoutes.stream().filter(s -> !merakasRoutesCodes.contains(s.getCode())).collect(Collectors.toList());

        log.info(String.format("Deleting %d routes.", routesToDelete.size()));
        for (Route route : routesToDelete) {
            log.info(String.format("Deleting '%s' route number '%s'.", route.getTransportType(), route.getNumber()));
            routeService.delete(route.getId());
        }

        if (!routesToDelete.isEmpty()) {
            storeImportInfo(cityId, DataImportEntity.ROUTE, Action.DELETE, (long) routesToDelete.size());
        }
    }

    private void storeRoutes(MerakasInfoWrapper merakasInfo, List<Route> currentRoutes, Long cityId, Long version) {
        List<Stop> stops = stopService.getStops(cityId);
        List<Route> routesToCreate = new ArrayList<>();
        List<Route> routesToUpdate = new ArrayList<>();

        for (MerakasRoute merakasRoute : merakasInfo.getRoutes()) {
            if (ILLEGAL_ROUTE_NAMES.stream().anyMatch(irn -> merakasRoute.getId().contains(irn))) {
                log.warn(String.format("Ignoring route with ID '%s'", merakasRoute.getId()));
                continue;
            }

            List<MerakasTrip> merakasTrips = merakasInfo.getTrips().stream().filter(t -> t.getRouteId().equals(merakasRoute.getId())).collect(Collectors.toList());
            List<String> merakasTripIds = merakasTrips.stream().map(MerakasTrip::getTripId).collect(Collectors.toList());
            List<MerakasStopTime> merakasStopTimes = merakasInfo.getStopTimes().stream().filter(st -> merakasTripIds.contains(st.getTripId())).collect(Collectors.toList());
            Route route = routeConverter.convert(merakasRoute, merakasTrips, merakasStopTimes, merakasInfo.getCalendars(), merakasInfo.getShapes(), stops, cityId);
            Route currentRoute = currentRoutes.stream().filter(r -> r.getCode().equals(route.getCode())).findFirst().orElse(null);
            if (currentRoute == null) {
                routesToCreate.add(route);
            } else if (haveChanges(currentRoute, route)) {
                routesToUpdate.add(route);
            }
        }

        log.info(String.format("Creating %d routes.", routesToCreate.size()));
        for (Route route : routesToCreate) {
            log.info(String.format("Creating '%s' route number '%s'.", route.getTransportType(), route.getNumber()));
            route.setVersion(version);
            route.setVersionDate(LocalDate.now());
            routeService.save(route);
        }

        if (!routesToCreate.isEmpty()) {
            storeImportInfo(cityId, DataImportEntity.ROUTE, Action.CREATE, (long) routesToCreate.size());
        }

        log.info(String.format("Updating %d routes.", routesToUpdate.size()));
        for (Route route : routesToUpdate) {
            log.info(String.format("Updating '%s' route number '%s'.", route.getTransportType(), route.getNumber()));
            currentRoutes.stream().filter(r -> r.getCode().equals(route.getCode())).findFirst().ifPresent(r -> route.setId(r.getId()));
            route.setVersion(version);
            route.setVersionDate(LocalDate.now());
            routeService.delete(route.getId());
            routeService.save(route);
        }

        if (!routesToUpdate.isEmpty()) {
            storeImportInfo(cityId, DataImportEntity.ROUTE, Action.UPDATE, (long) routesToUpdate.size());
        }

        log.info("Routes stored.");
    }

    private boolean haveChanges(Stop first, Stop second) {
        return !first.getCityId().equals(second.getCityId()) || !first.getTitle().equals(second.getTitle()) || first.getLatitude().compareTo(second.getLatitude()) != 0 || first.getLongitude().compareTo(second.getLongitude()) != 0;
    }

    private boolean haveChanges(Route first, Route second) {
        if (!first.getCityId().equals(second.getCityId()) || !first.getTransportType().equals(second.getTransportType()) || !first.getNumber().equals(second.getNumber()) || !first.getTitle().equals(second.getTitle()) || first.getDirections().size() != second.getDirections().size()) {
            return true;
        }

        for (int i = 0; i < first.getDirections().size(); i++) {
            Direction firstDirection = first.getDirections().get(i);
            Direction secondDirection = second.getDirections().get(i);

            if (!firstDirection.getCode().equals(secondDirection.getCode()) || !firstDirection.getTitle().equals(secondDirection.getTitle()) || !firstDirection.getSequence().equals(secondDirection.getSequence()) || firstDirection.getDirectionStops().size() != secondDirection.getDirectionStops().size() || firstDirection.getDirectionShapes().size() != secondDirection.getDirectionShapes().size()) {
                return true;
            }

            for (int j = 0; j < firstDirection.getDirectionStops().size(); j++) {
                DirectionStop firstDirectionStop = firstDirection.getDirectionStops().get(j);
                DirectionStop secondDirectionStop = secondDirection.getDirectionStops().get(j);

                if (!firstDirectionStop.getStop().getId().equals(secondDirectionStop.getStop().getId()) || !firstDirectionStop.getSequence().equals(secondDirectionStop.getSequence()) || firstDirectionStop.getStopTimes().size() != secondDirectionStop.getStopTimes().size()) {
                    return true;
                }

                for (int k = 0; k < firstDirectionStop.getStopTimes().size(); k++) {
                    StopTime firstStopTime = firstDirectionStop.getStopTimes().get(k);
                    StopTime secondStopTime = secondDirectionStop.getStopTimes().get(k);

                    if (!firstStopTime.getTripCode().equals(secondStopTime.getTripCode()) || !firstStopTime.getMonday().equals(secondStopTime.getMonday()) || !firstStopTime.getTuesday().equals(secondStopTime.getTuesday()) || !firstStopTime.getWednesday().equals(secondStopTime.getWednesday()) || !firstStopTime.getThursday().equals(secondStopTime.getThursday()) || !firstStopTime.getFriday().equals(secondStopTime.getFriday()) || !firstStopTime.getSaturday().equals(secondStopTime.getSaturday()) || !firstStopTime.getSunday().equals(secondStopTime.getSunday()) || !firstStopTime.getTime().equals(secondStopTime.getTime())) {
                        return true;
                    }
                }
            }

            for (int j = 0; j < firstDirection.getDirectionShapes().size(); j++) {
                DirectionShape firstDirectionShape = firstDirection.getDirectionShapes().get(j);
                DirectionShape secondDirectionShape = secondDirection.getDirectionShapes().get(j);

                if (firstDirectionShape.getLatitude().compareTo(secondDirectionShape.getLatitude()) != 0 || firstDirectionShape.getLongitude().compareTo(secondDirectionShape.getLongitude()) != 0 || !firstDirectionShape.getSequence().equals(secondDirectionShape.getSequence())) {
                    return true;
                }
            }
        }

        return false;
    }

    private void storeImportInfo(Long cityId, DataImportEntity entity, Action action, Long amount) {
        DataImport dataImport = new DataImport();
        dataImport.setDate(LocalDate.now());
        dataImport.setCityId(cityId);
        dataImport.setSystem(SchedulesScheduler.SYSTEM_MERAKAS);
        dataImport.setEntity(entity);
        dataImport.setAction(action);
        dataImport.setAmount(amount);

        dataImportRepository.save(dataImport);
    }
}
