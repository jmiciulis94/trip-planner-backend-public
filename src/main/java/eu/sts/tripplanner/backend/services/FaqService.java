package eu.sts.tripplanner.backend.services;

import eu.sts.tripplanner.backend.entities.FaqCategory;
import eu.sts.tripplanner.backend.repositories.FaqCategoryRepository;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class FaqService {

    private final FaqCategoryRepository faqCategoryRepository;

    public FaqService(FaqCategoryRepository faqCategoryRepository) {
        this.faqCategoryRepository = faqCategoryRepository;
    }

    public List<FaqCategory> getCategories() {
        return faqCategoryRepository.findAll();
    }
}
