package eu.sts.tripplanner.backend.services;

import eu.sts.tripplanner.backend.dto.StopDto;
import eu.sts.tripplanner.backend.entities.Stop;
import eu.sts.tripplanner.backend.repositories.StopRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StopService {

    private final StopRepository stopRepository;

    public StopService(StopRepository stopRepository) {
        this.stopRepository = stopRepository;
    }

    public List<Stop> getStops(Long cityId) {
        return stopRepository.findAllByCityId(cityId);
    }

    public List<StopDto> getStopsDto(Long cityId) {
        return stopRepository.findAllByCityId(cityId).stream().map(this::convertStop).collect(Collectors.toList());
    }

    @Transactional
    public void save(Stop stop) {
        stopRepository.save(stop);
    }

    @Transactional
    public void delete(Long id) {
        stopRepository.deleteById(id);
    }

    private StopDto convertStop(Stop stop) {
        return new StopDto(stop.getId(), stop.getCityId(), stop.getTitle(), stop.getLatitude(), stop.getLongitude(), stop.getVersion());
    }
}
