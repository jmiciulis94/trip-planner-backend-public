package eu.sts.tripplanner.backend.services;

import eu.sts.tripplanner.backend.constants.TransportType;
import eu.sts.tripplanner.backend.dto.TransportLocationDto;
import eu.sts.tripplanner.backend.entities.City;
import eu.sts.tripplanner.backend.entities.TransportLocation;
import eu.sts.tripplanner.backend.repositories.TransportLocationRepository;
import java.util.List;
import java.util.stream.Collectors;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service
public class TransportLocationService {

    private final TransportLocationRepository transportLocationRepository;

    public TransportLocationService(TransportLocationRepository transportLocationRepository) {
        this.transportLocationRepository = transportLocationRepository;
    }

    public List<TransportLocationDto> getTransportLocations(Long cityId) {
        List<TransportLocation> transportLocations = transportLocationRepository.findByCityId(cityId);
        return transportLocations.stream().map(this::convertTransportLocation).collect(Collectors.toList());
    }

    public List<TransportLocationDto> getTransportLocations(Long cityId, List<TransportType> transportTypes) {
        List<TransportLocation> transportLocations = transportLocationRepository.findByCityId(cityId);
        return transportLocations.stream().filter(t -> transportTypes.contains(t.getTransportType())).map(this::convertTransportLocation).collect(Collectors.toList());
    }

    @Transactional
    public void updateTransportLocations(City city, List<TransportLocation> transportLocations) {
        transportLocationRepository.deleteByCityId(city.getId());
        transportLocationRepository.saveAll(transportLocations);
    }

    private TransportLocationDto convertTransportLocation(TransportLocation transportLocation) {
        return new TransportLocationDto(transportLocation.getId(), transportLocation.getTitle(), transportLocation.getColor(), transportLocation.getLatitude(), transportLocation.getLongitude());
    }
}
