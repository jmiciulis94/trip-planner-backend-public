package eu.sts.tripplanner.backend.services;

import eu.sts.tripplanner.backend.dto.DirectionDto;
import eu.sts.tripplanner.backend.dto.DirectionShapeDto;
import eu.sts.tripplanner.backend.dto.DirectionStopDto;
import eu.sts.tripplanner.backend.dto.RouteDto;
import eu.sts.tripplanner.backend.dto.StopTimeDto;
import eu.sts.tripplanner.backend.entities.Direction;
import eu.sts.tripplanner.backend.entities.DirectionAlias;
import eu.sts.tripplanner.backend.entities.DirectionShape;
import eu.sts.tripplanner.backend.entities.DirectionStop;
import eu.sts.tripplanner.backend.entities.Route;
import eu.sts.tripplanner.backend.entities.RouteAlias;
import eu.sts.tripplanner.backend.entities.StopTime;
import eu.sts.tripplanner.backend.repositories.DirectionAliasRepository;
import eu.sts.tripplanner.backend.repositories.RouteAliasRepository;
import eu.sts.tripplanner.backend.repositories.RouteRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RouteService {

    private final RouteRepository routeRepository;
    private final RouteAliasRepository routeAliasRepository;
    private final DirectionAliasRepository directionAliasRepository;

    public RouteService(RouteRepository routeRepository, RouteAliasRepository routeAliasRepository, DirectionAliasRepository directionAliasRepository) {
        this.routeRepository = routeRepository;
        this.routeAliasRepository = routeAliasRepository;
        this.directionAliasRepository = directionAliasRepository;
    }

    public List<Route> getRoutes(Long cityId) {
        return routeRepository.findAllByCityId(cityId);
    }

    public List<RouteDto> getRoutesDto(Long cityId) {
        List<RouteAlias> routeAliases = routeAliasRepository.findAll();
        return routeRepository.findAllByCityId(cityId).stream().map(r -> convertRoute(r, routeAliases)).collect(Collectors.toList());
    }

    public List<DirectionDto> getDirections(Long routeId) {
        Route route = routeRepository.findById(routeId);
        List<DirectionAlias> directionAliases = directionAliasRepository.findAll();
        return route.getDirections().stream().map(d -> convertDirection(d, route.getId(), directionAliases)).collect(Collectors.toList());
    }

    public List<StopTimeDto> getStopTimes(Long id) {
        Route route = routeRepository.findById(id);

        List<StopTimeDto> stopTimes = new ArrayList<>();
        for (Direction direction : route.getDirections()) {
            for (DirectionStop directionStop : direction.getDirectionStops()) {
                for (StopTime stopTime : directionStop.getStopTimes()) {
                    stopTimes.add(convertStopTimes(stopTime, directionStop.getId()));
                }
            }
        }

        return stopTimes;
    }

    @Transactional
    public void save(Route route) {
        routeRepository.save(route);
    }

    @Transactional
    public void delete(Long id) {
        routeRepository.deleteById(id);
    }

    public Long getNextVersionNumber() {
        return routeRepository.getNextVersionNumber();
    }

    private RouteDto convertRoute(Route route, List<RouteAlias> routeAliases) {
        RouteAlias routeAlias = routeAliases.stream().filter(a -> a.getCode().equals(route.getCode()) && a.getTitle().equals(route.getTitle())).findFirst().orElse(null);
        return new RouteDto(route.getId(), route.getCityId(), route.getTransportType(), route.getNumber(), routeAlias != null ? routeAlias.getAlias() : route.getTitle(), route.getValidSince(), route.getVersion());
    }

    private DirectionDto convertDirection(Direction direction, Long routeId, List<DirectionAlias> directionAliases) {
        List<DirectionStopDto> directionStopsDto = direction.getDirectionStops().stream().map(this::convertDirectionStop).collect(Collectors.toList());
        List<DirectionShapeDto> directionShapesDto = direction.getDirectionShapes().stream().map(this::convertDirectionShape).collect(Collectors.toList());
        DirectionAlias directionAlias = directionAliases.stream().filter(a -> a.getDirectionCode().equals(direction.getCode()) && a.getTitle().equals(direction.getTitle())).findFirst().orElse(null);
        return new DirectionDto(direction.getId(), routeId, directionAlias != null ? directionAlias.getAlias() : direction.getTitle(), direction.getSequence(), directionStopsDto, directionShapesDto);
    }

    private DirectionStopDto convertDirectionStop(DirectionStop directionStop) {
        return new DirectionStopDto(directionStop.getId(), directionStop.getStop().getId(), directionStop.getSequence());
    }

    private StopTimeDto convertStopTimes(StopTime stopTime, Long directionStopId) {
        return new StopTimeDto(stopTime.getId(), directionStopId, stopTime.getTripCode(), stopTime.getMonday(), stopTime.getTuesday(), stopTime.getWednesday(), stopTime.getThursday(), stopTime.getFriday(), stopTime.getSaturday(), stopTime.getSunday(), stopTime.getTime());
    }

    private DirectionShapeDto convertDirectionShape(DirectionShape directionShape) {
        return new DirectionShapeDto(directionShape.getId(), directionShape.getLatitude(), directionShape.getLongitude(), directionShape.getSequence());
    }
}
