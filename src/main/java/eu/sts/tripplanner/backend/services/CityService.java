package eu.sts.tripplanner.backend.services;

import eu.sts.tripplanner.backend.dto.CityDto;
import eu.sts.tripplanner.backend.dto.CityTransportTypeDto;
import eu.sts.tripplanner.backend.entities.City;
import eu.sts.tripplanner.backend.entities.CityTransportType;
import eu.sts.tripplanner.backend.repositories.CityRepository;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;

@Service
public class CityService {

    private final CityRepository cityRepository;

    public CityService(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    public List<City> getCities() {
        return cityRepository.findAll();
    }

    public List<CityDto> getCitiesDto() {
        return cityRepository.findAll().stream().map(this::convertCity).collect(Collectors.toList());
    }

    private CityDto convertCity(City city) {
        List<CityTransportTypeDto> transportTypesDto = city.getTransportTypes().stream().map(this::convertCityTransportType).collect(Collectors.toList());
        return new CityDto(city.getId(), city.getName(), city.getSequence(), city.getSouthwestLatitude(), city.getSouthwestLongitude(), city.getNortheastLatitude(), city.getNortheastLongitude(), transportTypesDto);
    }

    private CityTransportTypeDto convertCityTransportType(CityTransportType cityTransportType) {
        return new CityTransportTypeDto(cityTransportType.getId(), cityTransportType.getTransportType(), cityTransportType.getColor());
    }
}
