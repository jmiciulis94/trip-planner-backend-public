package eu.sts.tripplanner.backend.services;

import eu.sts.tripplanner.backend.entities.TermsAndConditions;
import eu.sts.tripplanner.backend.repositories.TermsAndConditionsRepository;
import org.springframework.stereotype.Service;

@Service
public class TermsAndConditionsService {

    private final TermsAndConditionsRepository termsAndConditionsRepository;

    public TermsAndConditionsService(TermsAndConditionsRepository termsAndConditionsRepository) {
        this.termsAndConditionsRepository = termsAndConditionsRepository;
    }

    public TermsAndConditions getTermsAndConditions() {
        return termsAndConditionsRepository.findFirstByOrderByVersionDesc();
    }
}
