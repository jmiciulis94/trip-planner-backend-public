package eu.sts.tripplanner.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalTime;
import lombok.Data;

@Data
public class StopTimeDto {

    private final Long id;
    private final Long directionStopId;
    private final String tripCode;
    private final Boolean monday;
    private final Boolean tuesday;
    private final Boolean wednesday;
    private final Boolean thursday;
    private final Boolean friday;
    private final Boolean saturday;
    private final Boolean sunday;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm")
    private final LocalTime time;
}
