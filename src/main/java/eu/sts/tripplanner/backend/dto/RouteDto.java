package eu.sts.tripplanner.backend.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import eu.sts.tripplanner.backend.constants.TransportType;
import java.time.LocalDate;
import lombok.Data;

@Data
public class RouteDto {

    private final Long id;
    private final Long cityId;
    private final TransportType transportType;
    private final String number;
    private final String title;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private final LocalDate validSince;

    private final Long version;
}
