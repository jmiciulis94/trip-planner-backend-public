package eu.sts.tripplanner.backend.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class TransportLocationDto {

    private final String id;
    private final String title;
    private final String color;
    private final BigDecimal latitude;
    private final BigDecimal longitude;
}
