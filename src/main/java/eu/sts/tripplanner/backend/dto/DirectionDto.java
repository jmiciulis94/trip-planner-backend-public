package eu.sts.tripplanner.backend.dto;

import java.util.List;
import lombok.Data;

@Data
public class DirectionDto {

    private final Long id;
    private final Long routeId;
    private final String title;
    private final Long sequence;
    private final List<DirectionStopDto> directionStops;
    private final List<DirectionShapeDto> directionShapes;
}
