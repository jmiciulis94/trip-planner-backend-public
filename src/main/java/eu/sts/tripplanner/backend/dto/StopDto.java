package eu.sts.tripplanner.backend.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class StopDto {

    private final Long id;
    private final Long cityId;
    private final String title;
    private final BigDecimal latitude;
    private final BigDecimal longitude;
    private final Long version;
}
