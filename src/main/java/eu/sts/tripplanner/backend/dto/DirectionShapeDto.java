package eu.sts.tripplanner.backend.dto;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class DirectionShapeDto {

    private final Long id;
    private final BigDecimal latitude;
    private final BigDecimal longitude;
    private final Long sequence;
}
