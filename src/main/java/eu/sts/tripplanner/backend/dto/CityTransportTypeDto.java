package eu.sts.tripplanner.backend.dto;

import eu.sts.tripplanner.backend.constants.TransportType;
import lombok.Data;

@Data
public class CityTransportTypeDto {

    private final Long id;
    private final TransportType transportType;
    private final String color;
}
