package eu.sts.tripplanner.backend.dto;

import lombok.Data;

@Data
public class DirectionStopDto {

    private final Long id;
    private final Long stopId;
    private final Long sequence;
}
