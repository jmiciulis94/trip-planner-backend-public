package eu.sts.tripplanner.backend.dto;

import java.math.BigDecimal;
import java.util.List;
import lombok.Data;

@Data
public class CityDto {

    private final Long id;
    private final String name;
    private final Long sequence;
    private final BigDecimal southwestLatitude;
    private final BigDecimal southwestLongitude;
    private final BigDecimal northeastLatitude;
    private final BigDecimal northeastLongitude;
    private final List<CityTransportTypeDto> transportTypes;
}
