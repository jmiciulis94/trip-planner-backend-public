package eu.sts.tripplanner.backend.repositories;

import eu.sts.tripplanner.backend.entities.DataImportResult;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface DataImportResultRepository extends Repository<DataImportResult, Long> {

    void save(DataImportResult dataImportResult);
}
