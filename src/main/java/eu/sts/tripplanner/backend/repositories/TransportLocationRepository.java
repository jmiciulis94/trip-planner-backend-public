package eu.sts.tripplanner.backend.repositories;

import eu.sts.tripplanner.backend.entities.TransportLocation;
import java.util.List;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface TransportLocationRepository extends Repository<TransportLocation, Long> {

    List<TransportLocation> findByCityId(Long cityId);

    void deleteByCityId(Long cityId);

    void saveAll(Iterable<TransportLocation> transportLocations);
}
