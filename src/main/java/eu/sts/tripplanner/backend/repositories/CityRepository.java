package eu.sts.tripplanner.backend.repositories;

import eu.sts.tripplanner.backend.entities.City;
import java.util.List;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface CityRepository extends Repository<City, Long> {

    List<City> findAll();
}
