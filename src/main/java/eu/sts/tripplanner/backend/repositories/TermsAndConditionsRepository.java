package eu.sts.tripplanner.backend.repositories;

import eu.sts.tripplanner.backend.entities.TermsAndConditions;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface TermsAndConditionsRepository extends Repository<TermsAndConditions, Long> {

    TermsAndConditions findFirstByOrderByVersionDesc();
}
