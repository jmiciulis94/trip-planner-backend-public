package eu.sts.tripplanner.backend.repositories;

import eu.sts.tripplanner.backend.entities.Request;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface RequestRepository extends Repository<Request, Long> {

    void save(Request request);
}
