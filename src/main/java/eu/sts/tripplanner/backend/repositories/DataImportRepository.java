package eu.sts.tripplanner.backend.repositories;

import eu.sts.tripplanner.backend.entities.DataImport;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface DataImportRepository extends Repository<DataImport, Long> {

    void save(DataImport dataImport);
}
