package eu.sts.tripplanner.backend.repositories;

import eu.sts.tripplanner.backend.entities.RouteAlias;
import java.util.List;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface RouteAliasRepository extends Repository<RouteAlias, Long> {

    List<RouteAlias> findAll();
}
