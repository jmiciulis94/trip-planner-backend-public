package eu.sts.tripplanner.backend.repositories;

import eu.sts.tripplanner.backend.entities.Route;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface RouteRepository extends Repository<Route, Long> {

    Route findById(Long id);

    List<Route> findAllByCityId(Long cityId);

    void save(Route route);

    void deleteById(Long id);

    @Query(value = "SELECT nextval('version')", nativeQuery = true)
    Long getNextVersionNumber();
}
