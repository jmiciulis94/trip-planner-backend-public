package eu.sts.tripplanner.backend.repositories;

import eu.sts.tripplanner.backend.entities.Stop;
import java.util.List;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface StopRepository extends Repository<Stop, Long> {

    List<Stop> findAllByCityId(Long cityId);

    void save(Stop stop);

    void deleteById(Long id);
}
