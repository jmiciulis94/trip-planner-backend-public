package eu.sts.tripplanner.backend.repositories;

import eu.sts.tripplanner.backend.entities.DirectionAlias;
import java.util.List;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface DirectionAliasRepository extends Repository<DirectionAlias, Long> {

    List<DirectionAlias> findAll();
}
