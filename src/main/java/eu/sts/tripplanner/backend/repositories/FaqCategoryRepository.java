package eu.sts.tripplanner.backend.repositories;

import eu.sts.tripplanner.backend.entities.FaqCategory;
import java.util.List;
import org.springframework.data.repository.Repository;

@org.springframework.stereotype.Repository
public interface FaqCategoryRepository extends Repository<FaqCategory, Long> {

    List<FaqCategory> findAll();
}
