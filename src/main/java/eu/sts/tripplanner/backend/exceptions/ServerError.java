package eu.sts.tripplanner.backend.exceptions;

import java.util.List;
import lombok.Getter;

@Getter
public class ServerError {

    private final String header;
    private final List<String> items;

    public ServerError(String header, List<String> items) {
        this.header = header;
        this.items = items;
    }
}
