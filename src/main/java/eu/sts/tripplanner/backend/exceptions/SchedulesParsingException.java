package eu.sts.tripplanner.backend.exceptions;

public class SchedulesParsingException extends RuntimeException {

    public SchedulesParsingException(String message) {
        super(message);
    }
}
