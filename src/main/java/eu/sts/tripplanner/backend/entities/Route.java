package eu.sts.tripplanner.backend.entities;

import eu.sts.tripplanner.backend.constants.TransportType;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Getter
@Setter
@Entity(name = "Route")
@Table(name = "route")
public class Route {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "city_id", nullable = false)
    private Long cityId;

    @Column(name = "code", nullable = false, unique = true)
    private String code;

    @Column(name = "transport_type", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private TransportType transportType;

    @Column(name = "number", nullable = false, length = 10)
    private String number;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "valid_since")
    private LocalDate validSince;

    @JoinColumn(name = "route_id", referencedColumnName = "id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    @OrderBy(value = "sequence")
    private List<Direction> directions = new ArrayList<>();

    @Column(name = "version", nullable = false)
    private Long version;

    @Column(name = "version_date", nullable = false)
    private LocalDate versionDate;
}
