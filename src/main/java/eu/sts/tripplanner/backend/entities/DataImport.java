package eu.sts.tripplanner.backend.entities;

import eu.sts.tripplanner.backend.constants.Action;
import eu.sts.tripplanner.backend.constants.DataImportEntity;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "DataImport")
@Table(name = "data_import")
public class DataImport {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date", nullable = false)
    private LocalDate date;

    @JoinColumn(name = "city_id", nullable = false)
    private Long cityId;

    @Column(name = "system", nullable = false)
    private String system;

    @Column(name = "entity", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private DataImportEntity entity;

    @Column(name = "action", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Action action;

    @Column(name = "amount", nullable = false)
    private Long amount;
}
