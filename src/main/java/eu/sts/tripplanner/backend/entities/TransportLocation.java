package eu.sts.tripplanner.backend.entities;

import eu.sts.tripplanner.backend.constants.TransportType;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "TransportLocation")
@Table(name = "transport_location")
public class TransportLocation {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private String id;

    @JoinColumn(name = "city_id", nullable = false)
    private Long cityId;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "color", nullable = false)
    private String color;

    @Column(name = "transport_type", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private TransportType transportType;

    @Column(name = "latitude", nullable = false)
    private BigDecimal latitude;

    @Column(name = "longitude", nullable = false)
    private BigDecimal longitude;
}
