package eu.sts.tripplanner.backend.entities;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Getter
@Setter
@Entity(name = "TermsAndConditions")
@Table(name = "terms_and_conditions")
public class TermsAndConditions {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "version", nullable = false)
    private Integer version;

    @Column(name = "header_en", nullable = false)
    private String headerEn;

    @Column(name = "header_lt", nullable = false)
    private String headerLt;

    @Column(name = "footer_en", nullable = false)
    private String footerEn;

    @Column(name = "footer_lt", nullable = false)
    private String footerLt;

    @JoinColumn(name = "terms_and_conditions_id", referencedColumnName = "id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    @OrderBy(value = "sequence")
    private List<TermsAndConditionsChapter> chapters = new ArrayList<>();
}
