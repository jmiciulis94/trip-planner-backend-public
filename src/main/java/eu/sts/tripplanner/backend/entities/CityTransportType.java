package eu.sts.tripplanner.backend.entities;

import eu.sts.tripplanner.backend.constants.TransportType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "CityTransportType")
@Table(name = "city_transport_type")
public class CityTransportType {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private Long id;

    @Column(name = "transport_type", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private TransportType transportType;

    @Column(name = "color", nullable = false, length = 10)
    private String color;
}
