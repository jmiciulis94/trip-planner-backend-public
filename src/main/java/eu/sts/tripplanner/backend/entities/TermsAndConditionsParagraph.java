package eu.sts.tripplanner.backend.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "TermsAndConditionsParagraph")
@Table(name = "terms_and_conditions_paragraph")
public class TermsAndConditionsParagraph {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "text_en", nullable = false)
    private String textEn;

    @Column(name = "text_lt", nullable = false)
    private String textLt;

    @Column(name = "sequence", nullable = false)
    private Long sequence;
}
