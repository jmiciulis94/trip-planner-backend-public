package eu.sts.tripplanner.backend.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "FaqQuestion")
@Table(name = "faq_question")
public class FaqQuestion {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "question_en", nullable = false)
    private String questionEn;

    @Column(name = "question_lt", nullable = false)
    private String questionLt;

    @Column(name = "answer_en", nullable = false)
    private String answerEn;

    @Column(name = "answer_lt", nullable = false)
    private String answerLt;

    @Column(name = "sequence", nullable = false)
    private Long sequence;
}
