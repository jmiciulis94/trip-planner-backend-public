package eu.sts.tripplanner.backend.entities;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Getter
@Setter
@Entity(name = "City")
@Table(name = "city")
public class City {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @JoinColumn(name = "city_id", referencedColumnName = "id", nullable = false)
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<CityTransportType> transportTypes = new ArrayList<>();

    @Column(name = "sequence", nullable = false)
    private Long sequence;

    @Column(name = "southwest_latitude", nullable = false)
    private BigDecimal southwestLatitude;

    @Column(name = "southwest_longitude", nullable = false)
    private BigDecimal southwestLongitude;

    @Column(name = "northeast_latitude", nullable = false)
    private BigDecimal northeastLatitude;

    @Column(name = "northeast_longitude", nullable = false)
    private BigDecimal northeastLongitude;

    @Column(name = "schedules_url", nullable = false)
    private String schedulesUrl;

    @Column(name = "gps_url", nullable = false)
    private String gpsUrl;
}
