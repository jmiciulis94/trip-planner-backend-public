package eu.sts.tripplanner.backend.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "DirectionAlias")
@Table(name = "direction_alias")
public class DirectionAlias {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code", nullable = false, unique = true)
    private String directionCode;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "alias", nullable = false)
    private String alias;
}
