package eu.sts.tripplanner.backend.entities;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "DataImportResult")
@Table(name = "data_import_result")
public class DataImportResult {

    @Id
    @Column(name = "id", unique = true, nullable = false, updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "date", nullable = false)
    private LocalDate date;

    @JoinColumn(name = "city_id", nullable = false)
    private Long cityId;

    @Column(name = "system", nullable = false)
    private String system;

    @Column(name = "success", nullable = false)
    private Boolean success;
}
