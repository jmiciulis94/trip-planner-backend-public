# Trip Planner back-end

## Build Docker image

Build archive:

`./gradlew clean build`

Build Docker image:

`docker build -t sts/trip-planner-backend .`

## Deploy image to AWS

Authenticate Docker client on AWS:

`aws ecr get-login-password --region eu-central-1 | docker login --username AWS --password-stdin 637227582294.dkr.ecr.eu-central-1.amazonaws.com`

Tag image:

`docker tag sts/trip-planner-backend:latest 637227582294.dkr.ecr.eu-central-1.amazonaws.com/trip-planner-images:latest`

Push image to AWS repository:

`docker push 637227582294.dkr.ecr.eu-central-1.amazonaws.com/trip-planner-images:latest`
